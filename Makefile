all: dash reverse

dash: dash.c
	gcc -o dash dash.c

reverse: reverse.c
	gcc -o reverse reverse.c

clobber:
	rm dash reverse
