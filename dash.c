#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#include <sys/types.h>
#include "utils.h"

#define SHELL_PROMPT "$ "
#define WELCOME_MESSAGE "(D)avids (A)wful (SH)ell - An awful replacement for your shell.\n"

/*  Global ENV variables (initialized at startup by the OS)   */
char* PATH;
char* HOME;
char* TERM;
char WD[1024];
extern char** environ;
/*
 *	Displays a FATAL ERROR message plus the user passed msg then terminates the process
 */
void shell_exit(char* msg)
{
	char buf[ 15 + sizeof(msg) ];
	snprintf( buf, sizeof( buf ), "%s%s%s", "[FATAL ERROR] ", msg, "\n" );
	if(msg) fprintf( stderr, "%s", buf );
	exit(-1);
}

/*
 *  Changes the shells current working directory
 */
void shell_change_dir(char** args)
{
    if(args[1]) // if an argument is specified
    {
        if( chdir( args[1] ) ) printf( "DASH: cd: %s: No such directory\n", args[1] );
    }
    else
    {
        chdir( HOME );
    }
    getcwd(WD, 1024);
}

/*
 *	shell_read_line()
 *
 *	calls the getline function from stdio and returns a char*
 *	NOTE: getline allocates memory for the line which must be freed later
 */
char* shell_read_line()
{
	char* line = NULL;
	size_t bufsize = 0;
	getline( &line, &bufsize, stdin );
	return line;
}

/*
 *	shell_split_lines
 *
 *	splits line into an array of char* each pointing
 *	to a new word sperated by whitespace
 */
char** shell_split_line(char* line)
{
	const char* token_delims = " \t\r\n\a";

	int bufsize = 64; // yay magic numbers
	int position = 0;
	char** tokens = malloc( bufsize * sizeof( char* ) );
	char* token;

	if(!tokens) shell_exit( "DASH: Memory Allocation Error" );

	// begin tokenising string
	token = strtok( line, token_delims );
	while( token != NULL )
	{
		tokens[position++] = token;

		// double the buffersize if we have run out
		if( position >= bufsize )
		{
			bufsize *= 2;

			tokens = realloc( tokens, bufsize * sizeof( char* ) );
			if(!tokens) shell_exit( "DASH: Memory Reallocation Error" );
		}

		token = strtok( NULL, token_delims );
	}

	tokens[position] = NULL;
	return tokens;
}

/*
 *	shell_exceute
 *	forks process and calls execve() on args[0].
 *	parent process then waits for child to exit
 */
int shell_execute( char** args )
{
	int status = 0;
	pid_t pid;

	pid = fork();
	if( pid == 0 )
	{
		// child process code
        char* program = 0;

        if( shell_find_executable( args[0], PATH, &program ) ) // if we found program
        {
#ifdef DEBUG
            printf("%s\n", program);    // print the program string for debugging
#endif
            status = execve( program, args, environ );
            free( program );
        }
        else // didn't find program
        {
            printf( "DASH: Could not find executable %s\n", args[0] );
        }
		exit(0);
	} else if( pid < 0 )
	{
		// Error when forking
		shell_exit( "DASH: Fork Error" );
	} else {
		// Parent process code
		waitpid( -1, &status, 0 );
	}

	return status;
}

/*
 *	shell_loop
 *	main loop of shell program.
 */
void shell_loop()
{
	char*  line;
	char** args;
	int    status;

	do
	{
		printf( "%s %s", WD, SHELL_PROMPT );
		line = shell_read_line();
		args = shell_split_line( line );

        if( strcmp( args[0], "exit" ) == 0 ) { break; }
        if( strcmp( args[0], "cd") == 0 )
        {
            shell_change_dir( args );
            continue;
        }

		status = shell_execute( args );

		// free variables allocated by inner functions
		free( line );
		free( args );
	} while ( 1 );

	printf( "DASH: shell exiting with status %d\n", status );
}

int main()
{
    /* Load Config files etc. */
    printf( "%s", WELCOME_MESSAGE );
    PATH = getenv("PATH");
    HOME = getenv("HOME");
    TERM = getenv("TERM");
    getcwd(WD, 1024);
#ifdef DEBUG
    printf("HOME=%s\n", HOME);
    printf("PATH=%s\n", PATH);
#endif
    /* Enter main loop */
    shell_loop();

    /* shutdown/cleanup etc */
}
