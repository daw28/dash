#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/io.h>
#include <sys/mman.h>

int main(int argc, char* argv[])
{
    unsigned char *f, *g;
    int size;
    struct stat s;
    const char * file_name = argv[1];
    int fd = open(argv[1], O_RDWR);

    int status = fstat(fd, &s);
    size = s.st_size;
    int i, x;
    f = (char *) mmap (0, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
    g = f + (size -1);

    i = 0;
    while(i < (size/2))
    {
        char c = f[i];
        f[i] = *(g - i);
        *(g - i) = c;
        i++;
    }
#ifdef PRINTOUTPUT
    for(x = 0; x < size; x++)
    {
        printf("%c", f[x]);
    }
#endif
    munmap(f, size);
    close(fd);
}
